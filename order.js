var db = require('./db.js');
var data = require('./data.js');

function getOrder(id, cb) {
  db().all(`SELECT foods.id, foods.name, order_food.size, order_food.extras
            FROM order_food
            LEFT JOIN foods ON foods.id=foodID
            WHERE orderID = ?`, id, function(err, items) {
              db().get(`SELECT * FROM orders WHERE id = ?`, id, function(err, order) {
                cb(err, { order, items });
              });
            }).close();
}

module.exports = function(server) {
  server.get('/orders', function(req, res, next) {
    db().all(`SELECT * FROM orders
              WHERE completed IS NULL OR completed > strftime('%s','now') - 60 * 60
              ORDER BY (CASE WHEN completed IS NULL THEN 0 ELSE 1 END), created DESC
              LIMIT ?`, 10, function(err, rows) {
                res.send(rows);
              }).close();
  });

  server.get('/orders/open', function(req, res, next) {
    db().all(`SELECT * FROM orders WHERE completed IS NULL ORDER BY created ASC`, function(err, rows) {
      res.send(rows);
    }).close();
  });

  server.get('/orders/closed', function(req, res, next) {
    db().all('SELECT * FROM orders WHERE completed IS NOT NULL ORDER BY completed DESC LIMIT $limit', {
      $limit: 20
    }, function(err, rows) {
      res.send(rows);
    }).close();
  });

  server.get('/order/:id', function(req, res, next) {
    getOrder(req.params.id, function(err, row) {
      res.send(row);
    });
  });

  server.post('/order/:id', function(req, res, next) {
    var id = req.params.id,
        complete = req.params.complete,
        query;

    if (complete) {
      query = 'UPDATE orders SET completed = strftime("%s", "now") WHERE completed IS NULL AND id = ?';
    } else {
      query = 'UPDATE orders SET completed = NULL WHERE id = ?';
    }

    db().run(query, id, () => getOrder(id, (err, row) => res.send(row)));
  });

  server.put('/order', function(req, res, next) {
    var txn = db();
    var orderID;
    var notes = req.params.notes || '';
    txn.run('INSERT INTO orders (notes) VALUES (?)', notes, function() {
      orderID = this.lastID;
      if (orderID) {
        var statement = txn.prepare('INSERT INTO order_food (orderID, foodID, size, extras) VALUES (?, ?, ?, ?)');
        req.params.foods.forEach((food) => {
          var extras = food.extras || [];
          var extrasForFood = extras.map(extra => extra.name).join("|");
          statement.run(orderID, food.id, food.size, extrasForFood);
        });
        statement.finalize();
      }
    }).close(function() {
      res.send({
        order: orderID
      });
    });
    next();
  });
};
