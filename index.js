var restify = require('restify');

var db = require('./db.js');
var data = require('./data.js');


var server = restify.createServer();

server.use(restify.gzipResponse());
server.use(restify.bodyParser({ mapParams: true }));

server.get('/', function(req, res, next) {
  res.send('Hello Poutine');
});

server.get('/foods', function(req, res, next) {
  data.foods(foods => res.send(foods));
});

server.get('/extras', function(req, res, next) {
  data.extras(extras => res.send(extras));
});

require('./order.js')(server);

server.listen(23521);
