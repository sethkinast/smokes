var sqlite = require('sqlite3');

module.exports = function open() {
  return new sqlite.Database('data.db');
};

var db = new sqlite.Database('data.db');

db.run(`CREATE TABLE IF NOT EXISTS foods (
  id integer primary key,
  name text,
  category int
)`);

db.run(`CREATE TABLE IF NOT EXISTS orders (
  id integer primary key,
  notes text,
  created integer(4) default (strftime('%s', 'now')),
  completed integer(4)
)`);

db.run(`CREATE TABLE IF NOT EXISTS categories (
  id integer primary key,
  name text
)`);

db.run(`CREATE TABLE IF NOT EXISTS extras (
  id integer primary key,
  name text,
  category int
)`);

db.run(`CREATE TABLE IF NOT EXISTS order_food (
  foodID integer not null,
  orderID integer not null,
  size integer not null,
  extras text
)`);
