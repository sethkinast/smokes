var db = require('./db.js');

function foods(cb) {
  db().all(`SELECT foods.id, foods.name, foods.category AS cid, categories.name AS category FROM foods
            LEFT JOIN categories ON foods.category=categories.id
            ORDER BY cid ASC`,
  function(err, rows) {
    cb(rows);
  }).close();
}

function extras(cb) {
  db().all('SELECT id, name, category AS cid FROM extras ORDER BY cid ASC', function(err, rows) {
    cb(rows);
  }).close();
}

module.exports = {
  foods,
  extras
};
